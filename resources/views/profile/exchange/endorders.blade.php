@extends('layouts.profile')

@section('content')
    @if($user->role === \App\Models\User::ROLE_USER)
        @include('profile.roles.components.user-end')
    @else
        @include('profile.roles.components.prof-end')
    @endif
@endsection
