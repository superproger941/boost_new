<?php

$orders = \App\Models\Order::where('prof_id', \Auth::user()->id)
    ->where('status', \App\Models\Order::STATUS_SUCCESS)
    ->get();

$balanceErned = 0.00;
$balanceAvailable = \Auth::user()->balance;
$retention = 0.00;

foreach($orders as $order) {
    $balanceErned += (double)$order->amount;

    if((strtotime($order->updated_at)) > (time() - \App\Models\User::RETENTION_TIME)) {
        $balanceAvailable -= $order->amount;
    }
}

?>

<div class="content">
    <div class="pay-request-block">
        <form action="{{route('withdrawal.handler')}}" method="POST">
            @csrf
            <div class="log-modal-log2"> 
                <p>Sum</p>
                <input type="text" id="" value="" name="amount" placeholder="Set available sum" autocomplete="off">
            </div>
            <div class="log-modal-log3">
                <p>Payment Method</p>
                <input type="text" id="" value="" name="payment_system" placeholder="Visa, Quwi, PayPal" autocomplete="off">
            </div>
            <div class="log-modal-log3">
                <p>Payment Details</p>
                <input type="text" id="" value="" name="payment_info" placeholder="Card Number or Account" autocomplete="off">
            </div>
            <div class="log-modal-log3">
                <p>Your Available Sum</p>
                <div class="av-sum">
                    <p><span>available:</span> {{$balanceAvailable}}$</p>
                </div>
            </div>
            <div class="log-modal-log3">
                <p>Your Email</p>
                <div class="av-mail">
                    <p>{{\Auth::user()->email}}</p>
                </div>
            </div>
            <button class="reg-modal-log-login">Send Requests</button>
        </form>
    </div>
</div>