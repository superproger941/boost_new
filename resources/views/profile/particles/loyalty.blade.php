<?php

$level = (int)\Auth::user()->level;

function closest($number, $array){
    //infinite distance to start
    $dist = INF;
    //remember our last value
    $last = 1;

    foreach($array as $key => $v){
        //get our current distance
        $dist2 = abs($number - $v);

        //check if we are getting further than last number was
        if($dist2 > $dist){
            //return our last value
            return $last;
        }
        //set our new distance
        $dist = $dist2;
        //set our last value for next iteration
        $last = $key;
    }
    return $last;
}

$ordersResult = \App\Models\Order::where('client_id', \Auth::user()->id)->where('status', 2)->get();

$ordersCount = 0;
$ordersSpent = 0;

foreach($ordersResult as $order) {
    $ordersCount++;
    $ordersSpent += (int)$order->amount;
}

$loyaltyOrders = [];
$loyaltyNumbers = [];

foreach($loyalty as $item) {
    $loyaltyOrders[$item->id] = $item->orders_count;
    $loyaltyNumbers[$item->id] = $item->amount_count;
} unset($item);

$loyaltyOrdersId = closest($ordersCount, $loyaltyOrders);
$loyaltyNumbersId = closest($ordersSpent, $loyaltyNumbers);
$loyaltyId = $loyaltyNumbersId > $loyaltyOrdersId ? $loyaltyNumbersId : $loyaltyOrdersId;

?>
<loyalty  class="loyalprogram">
    <div class="loyalprogram-title">
        <img src="/assets/images/loyal.png" class="loyalprogram-title-ico">
        <h1>Loyalty Program</h1>
    </div>
    <div class="loyalprogram-blocks-aling">
        @foreach($loyalty as $item)
        <?php
            $check = $item->id === $loyaltyId;
            if($level < $item->id) {
                Auth::user()->level = $item->id;
                Auth::user()->bonunses = Auth::user()->bonunses + 50;
                Auth::user()->save();
            }
        ?>
        <div class="loyalprogram-blocks">
            <div class="loyalprogram-block">
                <p>{{$item->name}}</p>
                <img src="{{$check ? '/assets/images/loyalrang.gif' : '/assets/images/loyalrang.png' }}" class="loyalprogram-block-pic">
                <h2>{!!$item->description!!}</h2>
            </div>
            <div class="loyalprogram-block-text">
                <?php
                    $bonusHas = false;
                ?>
                <p>
                    Spent <?php if($item->amount_count): echo $item->amount_count.'$'; $bonusHas = true; endif; ?> <br> {{$bonusHas === true ? 'or' : ''}} {{$item->orders_count ? $item->orders_count : ''}} orders
                </p>
                @if($check)
                    <h2 class="active-h2">You Here</h2>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</loyalty>
