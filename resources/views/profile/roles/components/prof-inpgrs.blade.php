
<div class="boxed_wrapper">
    @include('profile.particles.header')
    <orderlist class="order-list">
        <div class="order-list-menu">
            @include('profile.particles.topmenu-pro-inp')
        </div>
        <div class="order-list-dyn-fix">
        @foreach($orders as $key => $order)
            @include('profile.roles.components.task-prof-inp')
        @endforeach
        </div>
    </orderlist>
    <div style="padding-bottom: 250px;"></div>
    @include('components.footer')
</div> 
