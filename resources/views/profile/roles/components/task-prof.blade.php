@if($order->status >= \App\Models\Order::STATUS_INPROGRESS)
<a href="{{route('order.show', ['id' => $order->id])}}">
@endif
    <div class="order-list-blocks-5">

        <div class="order-list-block">
            <p>Id:<span style="margin-left: 3px; color: white;">{{$key + 1}}</span></p>
        </div>
        <div class="order-list-block">
            <p>Game:<span style="margin-left: 3px; color: white;">{{$order->tarif->game->name}}</span></p>
        </div>
        <div class="order-list-block">
            <p>Service:<span style="margin-left: 3px; color: white;">{{$order->tarif->name}}</span></p>
        </div>
        <div class="order-list-block">
            <p>Platform:<span style="margin-left: 3px; color: white;">{{$order->tarif->platform->name}}</span></p>
        </div>
        <div class="order-list-block">
            <label for="hider2" class="clickme-button" id="clickme-{{$order->id}}">
                    <p>All Options</p>
            </label>
            <input type="checkbox" id="hider2">
            <div class="content2">
                <div class="additional-list-block">
                    @if($order->options)
                        @foreach (unserialize($order->options) as $option)
                        <div class="additional-list"> 
                            <p>{{$option['name']}}</p>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="order-list-block">
            <p>Price:<span style="margin-left: 3px; color: white;">${{$order->amount}}</span></p>
        </div>
        <div class="order-list-block">
            <p>Status:<span style="margin-left: 3px; color: white;">@include('profile.particles.status')</span></p>
        </div>
        <div class="order-list-block">
            <p>Progress:<span style="margin-left: 3px; color: white;">0%</span></p>
        </div>

        <form action="{{route('order.take')}}" method="POST" class="form-fix-1">
            @csrf
            <input type="hidden" value="{{$order->id}}" name="order_id">
            <button type="submit" id="take-btn">Take Order</button>
        </form>

<!--        
    <form action="{{route('order.take')}}" method="POST" class="form-fix-1">
            @csrf
            <input type="hidden" value="{{$order->id}}" name="order_id">
            <button type="submit" id="take-btn">Show Details</button>
    </form>
--->
    </div>
@if($order->status >= \App\Models\Order::STATUS_INPROGRESS)
</a>
@endif
