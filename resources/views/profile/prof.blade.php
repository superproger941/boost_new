
<div class="boxed_wrapper">
    <profile class="profile">

        <div class="profile-aling">

            <div class="profile-block-1">

                <img src="/assets/images/logo2.gif" class="footer-logo">

            </div>

            <div class="profile-menu">

                <div class="profile-avatar-aling">
                    <div class="profile-avatar-2">
                        <img class="profile-ava-3" src="/assets/images/avatars/1.png">
                    </div>
                </div>

                <div class="profile-menu-block">                
                    <h2>{{\Auth::user()->name}} | @include('components.role')</h2>
                    <a href="#">{{\Auth::user()->email}}</a>
                </div>


            </div>

            <div class="profile-menu">

                <div class="profile-avatar-aling">
                    <div class="profile-avatar">
                        <img class="profile-ava" src="/assets/images/balance.png">
                    </div>
                </div>

                <div class="balance-menu-block">

                    <h2>Balance:</h2>

                    <div class="blance-available-aling">

                        <div class="balance-available-4">
                            <p>Bonuce: <span style="color: white;">424 coins</span></p>
                        </div>

                        <div class="balance-available-2">
                            <p>Spend: <span style="color: white;">1,224$</span></p>
                        </div>

                    </div>

                </div>

                <div class="out-edit">

                    <div class="out">
                    <a href="{{route('logout')}}">Log out</a>
                    <img src="/assets/images/log.png" class="out-icon">
                    </div>
        
                    <div class="edit-prof">
                        <a href="#">Edit Profile</a>
                        <img src="/assets/images/edit.png" class="edit-prof-icon">
                    </div>
        
                </div>

            </div> 

        </div>


    </profile>

    <orderlist class="order-list">

        <div class="order-list-menu">

            <div class="order-list-menu-aling">

                <div class="order-pad-4">
                    <img src="/assets/images/progress.gif" class="order-icon">
                    <a href="#">In progress</a>
                </div>

                <div class="order-pad-4">
                    <img src="/assets/images/dragonico.png" class="order-icon">
                    <a href="#">End orders</a>
                </div>

                <div class="order-pad-3">
                    <img src="/assets/images/logo-an.gif" class="order-icon">
                    <a href="#">Buy more services</a>
                </div>

                <div class="order-pad-3">
                    <img src="/assets/images/dragonico.png" class="order-icon">
                    <a href="#">Back to home</a>
                </div>
    

            </div>

            <div class="order-title">
                <h1>order details</h1>
            </div>

        </div>

        <div class="order-list-margin">

            <div class="order-list-blocks-detail">

                <div class="order-list-block">
                    <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
                </div>
                <div class="order-list-block">
                    <p>Game:<span style="margin-left: 3px; color: white;">Apex Legend</span></p>
                </div>
                <div class="order-list-block">
                    <p>Service:<span style="margin-left: 3px; color: white;">Boost</span></p>
                </div>
                <div class="order-list-block">
                    <p>Platform:<span style="margin-left: 3px; color: white;">XBOX</span></p>
                </div>
                <div class="order-list-block">
                    <p>Additional<span style="margin-left: 3px; color: white;">></span></p>
                </div>
                <div class="order-list-block">
                    <p>Price:<span style="margin-left: 3px; color: white;">$25</span></p>
                </div>
                <div class="order-list-block">
                    <p>Status:<span style="margin-left: 3px; color: white;">Pending</span></p>
                </div>
                <div class="order-list-block">
                    <p>Progress:<span style="margin-left: 3px; color: white;">3800 rate</span></p>
                </div>

                <button type="submit" id="broke-btn">Broke Task</button>

            </div>

            <div class="show-details">

                <div class="show-details-aling">

                    <div class="chat">

                    <div id="chat-bg">

                    <ul id="messages"></ul>
                    <div class="messages-content"></div>

                    </div>

                    <form id="form" onsubmit="return sendMessage();">
                        <div class="input-chat">
                            <input id="message" placeholder="Write Message" autocomplete="off">
                            <button type="submit" id="send-chat">Send</button>
                        </div>
                    </form>

                    <div class="dit-progress-bar">

                        <div class="dit-pg-bar">
                            <img src="/assets/images/progress-dit.png" class="dit-progress-ico">
                            <p>Starting <br> <span>3800</span></p>
                        </div>

                        <div class="dit-pg-bar">
                            <img src="/assets/images/progress-dit.gif" class="dit-progress-ico">
                            <p class="active-dit-pg-bar">Currently <br> <span class="active-dit-pg-span">3800 rate</span></p>
                        </div>

                        <div class="dit-pg-bar">
                            <img src="/assets/images/progress-dit.png" class="dit-progress-ico">
                            <p>Purpose <br> <span>4800</span></p>
                        </div>


                    </div>

                </div>

                    <div class="dit-avatar">
                        <div class="dit-avatar-prof">
                            <img class="profile-ava-2" src="/assets/images/avatars/2.gif">
                        </div>
                        <div class="dit-avatar-name">
                            <h3>Doom</h3>
                            <p>Worked</p>
                        </div>
                        <div class="what-game">
                            <p>Apex Legend</p>
                        </div>
                    </div>

                </div>

            </div>

        </div>


    </orderlist>

    <loyalty  class="loyalprogram">

        <div class="loyalprogram-title">
            <img src="/assets/images/loyal.png" class="loyalprogram-title-ico">
            <h1>Loyalty Program</h1>
        </div>

        <div class="loyalprogram-blocks-aling">

            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 1</p>
                    <img src="/assets/images/loyalrang.gif" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2 class="active-h2">You Here</h2>
                </div>
            </div>

            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 2</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>

            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 3</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>

            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 4</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>

            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 5</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>

        </div>

    </loyalty>

    @include('components.footer')
</div> 

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>

<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-database.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
 https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-analytics.js"></script>

<script>
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
apiKey: "AIzaSyBgMTXJbeadYDEi32sv0eYlRvt2OHFacG0",
authDomain: "live-chat-8ca47.firebaseapp.com",
projectId: "live-chat-8ca47",
storageBucket: "live-chat-8ca47.appspot.com",
messagingSenderId: "804223580100",
appId: "1:804223580100:web:a4d14bee6e54dbac255691",
measurementId: "G-NRM0F0YGPB"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();


function sendMessage() {

var message = document.getElementById("message").value;

$('#form')[0].reset();

firebase.database().ref("messages").push().set({
    "sender": 1, 
    "message": message,
});
$("#chat-bg").scrollTop($("#chat-bg")[0].scrollHeight);
return false;

}

firebase.database().ref("messages").on("child_added", function (snapshot) {
if (snapshot.val().sender == 1) {
    var html = "";
    html += '<div class="chat-msg-toyou"><p>';
    html += /*snapshot.val().sender +*/ snapshot.val().message;
    html += "</p></div>";
} else {
    var html = "";
    html += '<div class="chat-msg-you"><p>';
    html += /*snapshot.val().sender +*/ snapshot.val().message;
    html += "</p></div>";
}
document.getElementById("messages").innerHTML += html;
});

</script>
