
<div class="boxed_wrapper">
    <profile class="profile">

        <div class="profile-aling">

            <div class="profile-block-1">

                <img src="/assets/images/details/logo.gif" class="footer-logo">

            </div>

            <div class="profile-menu">

                <div class="profile-avatar-aling">
                    <div class="profile-avatar">
                        <img class="profile-ava" src="/assets/images/balance.gif">
                    </div>
                </div>

                <div class="profile-menu-block">
                    <h2>{{\Auth::user()->name}} > @include('components.role')</h2>
                    <a href="{{\Auth::user()->email}}">{{\Auth::user()->email}}</a>
                </div>

            </div>

            <div class="profile-menu">

                <div class="profile-avatar-aling">
                    <div class="profile-avatar">
                        <img class="profile-ava" src="/assets/images/balance.png">
                    </div>
                </div>

                <div class="balance-menu-block">

                    <h2>Balance:</h2>

                    <div class="blance-available-aling">

                        <div class="balance-available">
                            <p>Available: <span style="color: white;">424$</span></p>
                        </div>

                        <div class="balance-available-2">
                            <p>Total: <span style="color: white;">1,224$</span></p>
                        </div>

                        <div class="balance-available-3">
                            <p>Earned: <span style="color: white;">1,224$</span></p>
                        </div>

                    </div>

                </div>

                <div class="out-edit">

                    <div class="out">
                    <a href="{{route('logout')}}">Log out</a>
                    <img src="/assets/images/log.png" class="out-icon">
                    </div>
        
                    <div class="edit-prof">
                        <a href="#">Edit Profile</a>
                        <img src="/assets/images/edit.png" class="edit-prof-icon">
                    </div>
        
                </div>

            </div> 

        </div>


    </profile>

    <orderlist class="order-list">

        <div class="order-list-menu">

            <div class="order-list-menu-aling">

                <div class="order-pad">
                    <img src="/assets/images/active.gif" class="order-icon">
                    <a href="allorders.html">All orders</a>
                </div>

                <div class="order-pad-2">
                    <img src="/assets/images/progress.png" class="order-icon">
                    <a href="inprogress.html">In progress</a>
                </div>

                <div class="order-pad-2">
                    <img src="/assets/images/dragonico.png" class="order-icon">
                    <a href="endorders.html">End orders</a>
                </div>

                <div class="order-pad-2">
                    <img src="/assets/images/dragonico.png" class="order-icon">
                    <a href="transactions.html">Transactions</a>
                </div>

                <div class="order-pad-2">
                    <img src="/assets/images/dragonico.png" class="order-icon">
                    <a href="#">Payment requests</a>
                </div>       

            </div>

            <div class="order-title">
                <h1>all orders</h1>
            </div>

        </div>

        <div class="order-list-blocks">

            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>Game:<span style="margin-left: 3px; color: white;">Apex Legend</span></p>
            </div>
            <div class="order-list-block">
                <p>Service:<span style="margin-left: 3px; color: white;">Boost</span></p>
            </div>
            <div class="order-list-block">
                <p>Platform:<span style="margin-left: 3px; color: white;">XBOX</span></p>
            </div>
            <div class="order-list-block">
                <p>Additional<span style="margin-left: 3px; color: white;">></span></p>
            </div>
            <div class="order-list-block">
                <p>Price:<span style="margin-left: 3px; color: white;">$25</span></p>
            </div>
            <div class="order-list-block">
                <p>Status:<span style="margin-left: 3px; color: white;">Pending</span></p>
            </div>
            <div class="order-list-block">
                <p>Progress:<span style="margin-left: 3px; color: white;">0%</span></p>
            </div>

            <button type="submit" id="take-btn">Take Order</button>

        </div>
        
        <div class="order-list-blocks-5">

            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>Game:<span style="margin-left: 3px; color: white;">Apex Legend</span></p>
            </div>
            <div class="order-list-block">
                <p>Service:<span style="margin-left: 3px; color: white;">Boost</span></p>
            </div>
            <div class="order-list-block">
                <p>Platform:<span style="margin-left: 3px; color: white;">XBOX</span></p>
            </div>
            <div class="order-list-block">
                <p>Additional<span style="margin-left: 3px; color: white;">></span></p>
            </div>
            <div class="order-list-block">
                <p>Price:<span style="margin-left: 3px; color: white;">$25</span></p>
            </div>
            <div class="order-list-block">
                <p>Status:<span style="margin-left: 3px; color: white;">Pending</span></p>
            </div>
            <div class="order-list-block">
                <p>Progress:<span style="margin-left: 3px; color: white;">0%</span></p>
            </div>

            <button type="submit" id="take-btn">Show Details</button>

        </div>

        <div class="order-list-blocks-2">

            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>Game:<span style="margin-left: 3px; color: white;">Apex Legend</span></p>
            </div>
            <div class="order-list-block">
                <p>Service:<span style="margin-left: 3px; color: white;">Boost</span></p>
            </div>
            <div class="order-list-block">
                <p>Platform:<span style="margin-left: 3px; color: white;">XBOX</span></p>
            </div>
            <div class="order-list-block">
                <p>Additional<span style="margin-left: 3px; color: white;">></span></p>
            </div>
            <div class="order-list-block">
                <p>Price:<span style="margin-left: 3px; color: white;">$25</span></p>
            </div>
            <div class="order-list-block">
                <p>Status:<span style="margin-left: 3px; color: white;">Pending</span></p>
            </div>
            <div class="order-list-block">
                <p>Progress:<span style="margin-left: 3px; color: white;">0%</span></p>
            </div>

        </div>

        <div class="order-list-blocks-3">

            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>You Received:<span style="margin-left: 3px; color: rgb(255, 86, 86);">224$</span></p>
            </div>
            <div class="order-list-block">
                <p>P.Date:<span style="margin-left: 3px; color: white;">11.11.21</span></p>
            </div>
            <div class="order-list-block">
                <div class="screen-shot"></div>
                <p>Screenshot</p>
            </div>
            <div class="order-list-block">
                <p>Pay:<span style="margin-left: 3px; color: white;">4441 4444 4442 3333</span></p>
            </div>
            <div class="order-list-block">
                <p>Comment:<span style="margin-left: 3px; color: white;">Thank for work!</span></p>
            </div>

        </div>

        <div class="order-list-blocks-4">

            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>You earned:<span style="margin-left: 3px; color: rgb(92, 255, 86);">1,224$</span></p>
            </div>
            <img src="/assets/images/details/load.gif" style="pointer-events: none;">
            <div class="order-list-block">
                <p>Please:<span style="margin-left: 3px; color: white;">always do job well</span></p>
            </div>


        </div>
        <div class="order-pads">
            <div class="order-pad-home">
                <a href="">Buy more Services</a>
                <img src="/assets/images/features.png" class="order-pad-home-img">
            </div>
            <div class="order-pad-home">
                <a href="/">Back to Home</a>
                <img src="/assets/images/features.png" class="order-pad-home-img">
            </div>
        </div>
    </orderlist>

    @include('components.footer')

</div> 
