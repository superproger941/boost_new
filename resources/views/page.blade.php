@extends('layouts.index')
@section('content')

<link rel="icon" type="image/png" href="assets/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/images/favicon/favicon-16x16.png" sizes="16x16">

<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')

    {!! $content !!}
    
    @include('components.footer')
</div> 
@endsection
