<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomFieldsToTarifs extends Migration
{
    public function up()
    {
        Schema::table('tarifs', function (Blueprint $table) {
            $table->json('custom_fields')->nullable();
        });
    }

    public function down()
    {
        Schema::table('tarifs', function (Blueprint $table) {
            //
        });
    }
}
