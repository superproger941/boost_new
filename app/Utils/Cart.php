<?php namespace App\Utils;

class Cart {
    // public static function getSignatureStaticCart(string $jwtToken)
    // {
    //     dd($jwtToken);
    //     $curl = curl_init();

    //     $products          = config('examples.1.products');
    //     $payload           = new \stdClass;
    //     $payload->merchant = '250907213813';
    //     $payload->lock     = 1;
    //     $payload->products = $products;
    //     $payload->currency = 'USD';

    //     curl_setopt_array($curl, [
    //         CURLOPT_URL            => 'https://secure.2checkout.com/checkout/api/encrypt/generate/signature',
    //         CURLOPT_RETURNTRANSFER => true,
    //         CURLOPT_CUSTOMREQUEST  => 'POST',
    //         CURLOPT_POSTFIELDS     => json_encode($payload),
    //         CURLOPT_HTTPHEADER     => [
    //             'content-type: application/json',
    //             'merchant-token: ' . $jwtToken,
    //         ],
    //     ]);
    //     $response = curl_exec($curl);
    //     $err      = curl_error($curl);
    //     curl_close($curl);
    //     if ($err) {
    //         throw new \Error('Curl error: ' . $err);
    //     }
    //     dd($response);
    //     $signature = self::parse($response);

    //     return $signature;
    // }

    public static function getSignatureForTheEntirePayload(string $jwtToken, \stdClass $cartPayload)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL            => 'https://secure.2checkout.com/checkout/api/encrypt/generate/signature',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => json_encode($cartPayload),
            CURLOPT_HTTPHEADER     => [
                'content-type: application/json',
                'merchant-token: ' . $jwtToken,
            ],
        ]);
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        // if ($err) {
        //     throw new \Error('Curl error: ' . $err);
        // }
        dd($response, $err);
        $signature = self::parse($response);

        return $signature;
    }

    public static function dynamic($jwtToken) {
        $cartPayload           = new \stdClass;
        $cartPayload->merchant = '250907213813';
        $cartPayload->currency = 'USD';
        $cartPayload->dynamic  = '1';
        // $cartPayload->sandbox  = '1';
        $cartPayload->products = [
            [
               "type" => "digital",
               "name" => "1",
               "quantity" => "1",
               "price" => "1"
            ]
         ]; //config('examples.5.products');

        // $jwtToken = JwtToken::getToken();
        $signature = static::getSignatureForTheEntirePayload($jwtToken, $cartPayload);
    }
}