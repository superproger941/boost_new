<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    public $table = 'chat';

    public $fillable = [
        'message',
        'order_id',
        'to_id',
        'from_id',
        'created_at',
        'updated_at',
    ];

    public function to() {
        return $this->belongsTo(\App\Models\User::class, 'id', 'to_id');
    }

    public function from() {
        return $this->belongsTo(\App\Models\User::class, 'id', 'from_id');
    }

    public function order() {
        return $this->belongsTo(\App\Models\Order::class);
    }
}
