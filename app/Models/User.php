<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use \App\Models\Order;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    const ROLE_USER = 0;
    const ROLE_PROF = 1;
    const ROLE_ADMIN = 2;
    const BONUS_RATE = 2;
    const RETENTION_TIME = 7 * 24 * 60 * 60; //7 days

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'balance',
        'bonunses',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function retention($user_id) {
        $orders = \App\Models\Order::where('prof_id', $user_id)
            ->where('status', \App\Models\Order::STATUS_SUCCESS)
            ->get();

        $retention = 0.00;
        foreach($orders as $order) {
            if((strtotime($order->updated_at)) > (time() - static::RETENTION_TIME)) {
                $retention += $order->amount;
            }
        }

        return $retention;
    }
}
