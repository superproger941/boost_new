<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_INPROGRESS = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILURE = 3;
    const MAX_FOR_ONE_USER = 3;

    public $fillable = [
        'client_id',
        'prof_id',
        'tarif_id',
        'status',
        'min_rating',
        'max_rating',
        'options',
        'bonus',
        'amount',
        'account_login',
        'account_password',
        'account_country',
    ];

    public function client() {
        return $this->belongsTo(\App\Models\User::class, 'client_id', 'id');
    }

    public function prof() {
        return $this->belongsTo(\App\Models\User::class, 'prof_id', 'id');
    }

    public function tarif() {
        return $this->belongsTo(\App\Models\Tarif::class, 'tarif_id', 'id');
    }

    public function options()
    {
        return $this->belongsToMany(\App\Models\Option::class, 'orders_options', 'order_id', 'option_id');
    }

    public function choosen()
    {
        return $this->hasMany(\App\Models\OrderOption::class, 'order_id', 'id');
    }

    public function progress() {
        return $this->hasMany(\App\Models\Progress::class);
    }

}
