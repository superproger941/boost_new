<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Form\FormElements;

class Tarifs extends Section implements Initializable
{
    /**
     * @var \App\Role
     */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-group';
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getTitle()
    {
        return 'Products';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                [
                    AdminColumn::text('id', 'ID')->setWidth('30px'),
                    AdminColumn::text('name', 'Name')->setWidth('30px'),
                    AdminColumn::text('price', 'Price')->setWidth('30px'),
                    AdminColumn::text('game.name', 'Game')->setWidth('30px'),
                    AdminColumn::text('platform.name', 'Platform')->setWidth('30px'),
                ]
            )->setApply(function ($query) {
                $query->orderBy('id', 'DESC');
            })->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $tabs = AdminDisplay::tabbed();

        $tabs->setTabs(function ($id) {
            $tabs = [];

            $tabs[] = AdminDisplay::tab(new FormElements([
                AdminFormElement::select('game_id', 'Game', \App\Models\Game::class)
                    ->setDisplay('name'),
                AdminFormElement::select('platform_id', 'Platform', \App\Models\Platform::class)
                    ->setDisplay('name'),
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::number('price', 'Price')->required(),
                AdminFormElement::text('price_for_rating', 'Price 4 Rating')->required(),

                AdminFormElement::number('bonus', 'Bonus (take)')->required(),
                AdminFormElement::number('bonus_cost', 'Bonus (cost)')->required(),

                AdminFormElement::text('max_rating', 'Max.Rating')->required(),
                AdminFormElement::textarea('description', 'Description'),

                AdminFormElement::image('background', 'Background'),

                AdminFormElement::view('admin.tarifs.custom_fields_script')
            ]))->setLabel('Common');

            return $tabs;
        });

        $form = AdminForm::panel()
            ->addHeader([
                $tabs
            ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    public function getCreateTitle()
    {
        return 'Add';
    }

    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return false;
    }

    public function isCreatable()
    {
        return true;
    }

    public function isEditable(\Illuminate\Database\Eloquent\Model $model)
    {
        return true;
    }
}