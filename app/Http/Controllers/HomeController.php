<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\User;
use App\Models\Bonus;
use App\Models\Option;
use App\Models\Tarif;
use App\Models\Withdrawal;
use App\Models\Chat;
use App\Models\Progress;
use View;
use Auth;
use DB;
use Arr;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware(['auth','verified']);

        View::share('loyalty', Bonus::get());
    }

    public function index()
    {
        $user = Auth::user();
        $is_user = (!$user->role);
        
        $orders = Order::where('status', \App\Models\Order::STATUS_CREATED)
            ->with('options', 'client', 'prof', 'tarif', 'tarif.game');

        if($is_user) {
            $orders = $orders->where('client_id', $user->id);
        }

        $orders = $orders->get();

        return view('profile.index', compact('user', 'orders'));
    }

    public function inprogress() {
        $user = Auth::user();
        $is_user = (!$user->role);
        
        $orders = Order::select()
            ->with('options', 'client', 'prof', 'tarif', 'tarif.game');

        if($is_user) {
            $orders = $orders->where('client_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_INPROGRESS);
        } else {
            $orders = $orders->where('prof_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_INPROGRESS);
        }

        $orders = $orders->get();

        return view('profile.exchange.inprogress', compact('user', 'orders'));
    }

    public function endorders() {
        $user = Auth::user();
        $is_user = (!$user->role);
        
        $orders = Order::select()
            ->with('options', 'client', 'prof', 'tarif', 'tarif.game');

        if($is_user) {
            $orders = $orders
                ->where('client_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_SUCCESS)
                ->orWhere('client_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_FAILURE);
        } else {
            $orders = $orders
                ->where('prof_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_SUCCESS)
                ->orWhere('prof_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_FAILURE);
        }

        $orders = $orders->get();

        return view('profile.exchange.endorders', compact('user', 'orders'));
    }

    public function transactions() {
        $user = Auth::user();
        $withdrawals = Withdrawal::where('user_id', $user->id)->get();
        
        return view('profile.balance.transactions', compact('user', 'withdrawals'));
    }

    public function paymentrequests() {
        $user = Auth::user();

        return view('profile.balance.payment_request', compact('user'));
    }

    public function showOrder(Request $request)
    {
        $user = Auth::user();
        $id = $request->id;
        $is_user = (!$user->role);

        $order = Order::select()->where('id', $id);
        
        if($is_user) {
            $order = $order->where('client_id', $user->id);
        } else {
            $order = $order->where('prof_id', $user->id);
        }


        $order = $order->firstOrFail();

        $progressItems = $order->progress()->orderBy('id', 'DESC')->get();
        $progress = null;
        if(count($progressArray = $progressItems->toArray())) {
            $progress = Arr::get($progressArray, '0.status');
        }

        $messages = Chat::where('order_id', $order->id)->orderBy('id', 'DESC')->get();
        $messages = array_reverse($messages->toArray());

        return view('profile.order', compact('user', 'order', 'progress', 'messages'));
    }

    public function takeOrder(Request $request) {
        $data = $request->all();
        $user = Auth::user();

        $order = Order::where('status', Order::STATUS_CREATED)
            ->where('id', \Arr::get($data, 'order_id'))
            ->firstOrFail();

        $orderOrdersInprogress = Order::where('prof_id', $user->id)
            ->where('status', Order::STATUS_INPROGRESS)
            ->count();
        
        if($orderOrdersInprogress >= Order::MAX_FOR_ONE_USER) {
            return back()->withSuccess('You exceeded limits.');
        }

        $order->status = Order::STATUS_INPROGRESS;
        $order->prof_id = $user->id;
        $order->save();

        return back()->withSuccess('Has been taken.');
    }

    public function endOrder(Request $request) {
        $data = $request->all();
        $user = Auth::user();

        $order = Order::where('status', Order::STATUS_INPROGRESS)
            ->where('id', Arr::get($data, 'order_id'))
            ->firstOrFail();

        DB::transaction(function() use ($order, $user) {
            $user->balance += $order->amount;
            $user->save();

            $order->client->balance -= $order->amount;
            $order->client->save();
            // dd($order->client);

            $order->status = Order::STATUS_SUCCESS;
            $order->save();
        });

        return back()->withSuccess('Has been closed.');
    }

    public function brokeOrder(Request $request) {
        $data = $request->all();
        $user = Auth::user();

        $order = Order::where('status', Order::STATUS_INPROGRESS)
            ->where('id', Arr::get($data, 'order_id'))
            ->firstOrFail();

        $order->status = Order::STATUS_FAILURE;
        $order->save();

        return back()->withSuccess('Has been broken.');
    }

    public function editProfileHandler(Request $request) {
        $data = $request->all();
        $user = Auth::user();

        $hasBennEdit = false;
        if($email = Arr::get($data, 'email')) {
            $user->email = $email;
            $hasBennEdit = true;
        }

        if($name = Arr::get($data, 'name')) {
            $user->name = $name;
            $hasBennEdit = true;
        }

        if($password = Arr::get($data, 'password')) {
            $user->password = \Hash::make($password);
            $hasBennEdit = true;
        }

        if($request->hasFile('file')) {
            $path = $request->file->store('avatars');
            $user->avatar = $path;
            $hasBennEdit = true;
        }

        if($hasBennEdit === true) {
            $user->save();
        }

        return back()->withSuccess('Profile has been edit.');
    }

    public function withdrawalHandler(Request $request) {
        $user = Auth::user();
        $data = $request->all();
        $data['user_id'] = $user->id;
        unset($data['_token']);
	
	$error = 0;
        DB::transaction(function() use ($data, $user, &$error) {
            $user->balance -= Arr::get($data, 'amount');

	    if($user->balance >= 0) {
		$user->save();

            	$result = Withdrawal::insert($data);
	    } else {
	    	DB::rollback();
		//$error = 1;
	    }
        });

	//if($error === 1) {
	//	return back()->withError('You don\'t have more balance');
	//}

        return back()->withSuccess('Your withdrawal has been created');
    }

    public function progressHandler(Request $request) {
        $user = Auth::user();
        $data = $request->all();
        unset($data['_token']);

        Progress::insert($data);

        return back()->withSuccess('Progress has been updated');
    }

    public function orderHandler(OrderRequest $request) {
        $user = Auth::user();
        $data = $request->all();
        $options = Arr::get($data, 'OPTIONS');
        unset($data['OPTIONS']);
        unset($data['_token']);
        $data['client_id'] = $user->id;

        $maxRating = Arr::get($data, 'max_rating');
        $minRating = Arr::get($data, 'min_rating');
        $rating = $maxRating - $minRating;

        // dd($data);
        
        $tarif = Tarif::where('id', Arr::get($data, 'tarif_id'))->firstOrFail();
        $priceForRating = $rating * $tarif->price_for_rating;
        $priceValue = $tarif->price;

        $bonuses = $user->bonuses > 0 ? $user->bonuses/User::BONUS_RATE : 0;

        // $optionsItems = Option::whereIn('id', array_keys($options))->get();
        $optionsItems = [];
        if($options && count($options)) {
            $optionsItems = Option::whereIn('id', array_keys($options))->get();
            $data['options'] = serialize($optionsItems->toArray());
        }
        // $data['options'] = serialize($optionsItems->toArray());
        $data['bonus'] = (double)$tarif->bonus;
        $data['amount'] += $priceForRating;

        foreach($optionsItems as $optionsItem) {
            $data['amount'] += (double)$optionsItem->amount;
            $data['bonus'] += (double)$optionsItem->bonus;
        }

        DB::transaction(function() use ($user, $data, $options) {
            $user->bonunses += Arr::get($data, 'bonus');
            $user->balance += Arr::get($data, 'amount');
            $user->save();

            $order = new Order;
            $order->fill($data);
            $order->save();
        });
        
        return redirect(route('home'))->withSuccess('Order has been maked.');
    }

    public function orderBonusHandler(OrderRequest $request) {
        $user = Auth::user();
        $data = $request->all();
        $options = Arr::get($data, 'OPTIONS');
        unset($data['OPTIONS']);
        unset($data['_token']);
        $data['client_id'] = $user->id;
        
        $tarif = Tarif::where('id', Arr::get($data, 'tarif_id'))->firstOrFail();
        $bonuses = $user->bonunses > 0 ? $user->bonunses : 0;

        $optionsItems = [];
        if($options && count($options)) {
            $optionsItems = Option::whereIn('id', array_keys($options))->get();
            $data['options'] = serialize($optionsItems->toArray());
        }
        $data['bonus'] = (double)$tarif->bonus;
        
        foreach($optionsItems as $optionsItem) {
            $data['amount'] += (double)$optionsItem->amount;
            $data['bonus'] += (double)$optionsItem->bonus;
        }

        if($tarif->bonus_cost > $user->bonunses) {
            return back()->withSuccess('You don\'t have nedeed bonuses.');
        }
        
        DB::transaction(function() use ($user, $data, $options, $tarif) {
            $user->bonunses -= $tarif->bonus_cost;
            $user->bonunses += Arr::get($data, 'bonus');
            $user->save();

            $data['comment'] = 'Order was bought for bonuses.';

            $order = new Order;
            $order->fill($data);
            $order->save();
        });
        
        return redirect(route('home'))->withSuccess('Order has been maked.');
    }

    public function addMessage(Request $request) {
        $data = $request->all();
        
        $user = Auth::user();
        $order = Order::where('id', Arr::get($data, 'order_id'))->firstOrFail();
        
        unset($data['_token']);
        $data['from_id'] = $user->id;
        $data['to_id'] = $user->id === $order->client_id ? $order->prof_id : $order->client_id;

        $chat = new Chat;
        $chat->fill($data);
        $chat->save();

        // dd($chat);

        return back()->withSuccess('Message has been sent.');
    }
}
