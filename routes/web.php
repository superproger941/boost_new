<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\MainController::class, 'show'])
    ->name('main');
Route::get('/contacts', [App\Http\Controllers\ContactController::class, 'show'])
    ->name('contacts');
Route::post('/contact', [App\Http\Controllers\ContactController::class, 'send'])
    ->name('contact.send');
Route::get('/game/{name}', [App\Http\Controllers\GamesController::class, 'show'])
    ->name('game');
Route::get('/product/{id}', [App\Http\Controllers\GamesController::class, 'product'])
    ->name('product');
Route::post('/order', [App\Http\Controllers\HomeController::class, 'orderHandler'])
    ->name('order');
Route::post('/order/bonus', [App\Http\Controllers\HomeController::class, 'orderBonusHandler'])
    ->name('order.bonus');
Route::get('/blog', [App\Http\Controllers\MainController::class, 'blogs'])
    ->name('blog');
Route::get('/blog/{slug}', [App\Http\Controllers\MainController::class, 'blog'])
    ->name('blog.item');
Route::get('/reviews', [App\Http\Controllers\MainController::class, 'reviews'])
    ->name('reviews');
Route::get('/join', [App\Http\Controllers\JoinController::class, 'show'])
    ->name('join');

require_once __DIR__ . '/web/auth.php';
require_once __DIR__ . '/web/profile.php';

Route::get('/test', function() {
    // $jwt = new \App\Utils\Jwt;
    // $token = $jwt->token();
    // $signature = \App\Utils\Cart::dynamic($token);

    // dd($token, $signature);

    // $merchantCode = "250907213813";
    // $key = "?A%yT4LBKXxQ~NJ]!g1("; 

    // $date = gmdate('Y-m-d H:i:s');
    // $string = strlen($merchantCode) . $merchantCode . strlen($date) . $date;
    // $hash = hash_hmac('md5', $string, $key);

    // $apiVersion = '6.0';
    // $resource = 'leads';
    // $host = "https://api.2checkout.com/rest/".$apiVersion."/".$resource."/";

    // $payload = '';

    // $ch = curl_init();

    // $headerArray = array(
    //     "Content-Type: application/json",
    //     "Accept: application/json",
    //     "X-Avangate-Authentication: code=\"{$merchantCode}\" date=\"{$date}\" hash=\"{$hash}\"",
    //     'Cookie: XDEBUG_SESSION=PHPSTORM'
    // );

    // curl_setopt($ch, CURLOPT_URL, $host);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    // curl_setopt($ch, CURLOPT_HEADER, FALSE);
    // curl_setopt($ch, CURLOPT_POST, FALSE);
    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    // curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    // curl_setopt($ch, CURLOPT_SSLVERSION, 0);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

    // $response = curl_exec($ch);

    // dd($response);
    // echo "<form action='https://www.2checkout.com/checkout/purchase' method='post'>
    //     <input type='hidden' name='sid' value='250907213813' />
    //     <input type='hidden' name='mode' value='2CO' />
    //     <input type='hidden' name='li_0_name' value='test' />
    //     <input type='hidden' name='li_0_price' value='0.01' />
    //     <input type='hidden' name='demo' value='Y' />
    //     <input name='submit' type='submit' value='Checkout' />
    // </form>";
});

// require_once(__DIR__ . "/lib/Twocheckout.php");

Route::post('/payment/success', function() {
    dd($_POST);
});

Route::get('{slug}', [App\Http\Controllers\MainController::class, 'page'])
    ->where(['slug' => '((?!admin).)*'])
    ->name('page');