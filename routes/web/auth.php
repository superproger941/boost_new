<?php

Auth::routes(['verify' => true]);

Route::get('/logout', function() {
    \Auth::logout();
    
    return redirect('/');
});

Route::get('/login', function() {
    return redirect('/');
})->name('login');

Route::get('register', function() {
    return redirect('/');
})->name('register');

Route::get('/email/verify', function() {
    return view('errors.email_verification');
})->name('verification.notice');
